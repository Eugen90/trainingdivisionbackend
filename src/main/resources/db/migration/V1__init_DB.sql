create table education_document (
    document_id bigint not null auto_increment,
    institution_name varchar(70) not null,
    issued_date DATE not null,
    name varchar(50) not null,
    number varchar(20) not null,
    primary key (document_id)) engine=InnoDB;

create table education_plan (
    education_plan_id bigint not null auto_increment,
    is_nine_base bit not null,
    year integer not null,
    specialty_id bigint,
    primary key (education_plan_id)) engine=InnoDB;

create table grade (
    grade_id bigint not null auto_increment,
    mark smallint not null,
    lesson_id bigint,
    student_id bigint,
    primary key (grade_id)) engine=InnoDB;

create table lesson (
    lesson_id bigint not null auto_increment,
    hour_count integer not null,
    name varchar(200) not null,
    semester_number smallint not null,
    education_plan_id bigint,
    primary key (lesson_id)) engine=InnoDB;

create table orders (
    order_id bigint not null auto_increment,
    date DATE not null,
    name varchar(22) not null,
    number varchar(25) not null,
    primary key (order_id)) engine=InnoDB;

create table organisation (
    organisation_id smallint not null auto_increment,
    abbreviation varchar(10) not null,
    accreditation_certificate varchar(40) not null,
    address varchar(80) not null,
    designation varchar(80) not null,
    director varchar(40) not null,
    director_position varchar(40) not null,
    email varchar(255) not null,
    license varchar(40) not null,
    name varchar(50) not null,
    post_index varchar(8) not null,
    telephone varchar(40) not null,
    website varchar(50) not null,
    primary key (organisation_id)) engine=InnoDB;

create table parent (
    parent_id bigint not null auto_increment,
    name varchar(100) not null,
    status varchar(10) not null,
    telephone varchar(18),
    student_id bigint,
    primary key (parent_id)) engine=InnoDB;

create table passport (
    passport_id bigint not null auto_increment,
    issued_by varchar(150) not null,
    issued_date DATE not null,
    number varchar(12) not null,
    primary key (passport_id)) engine=InnoDB;

create table specialty (
    specialty_id bigint not null auto_increment,
    ending_date DATE not null,
    name varchar(150) not null,
    training_length smallint not null,
    primary key (specialty_id)) engine=InnoDB;

create table status (
    status_id bigint not null auto_increment,
    name varchar(17) not null,
    primary key (status_id)) engine=InnoDB;

create table student (
    id bigint not null auto_increment,
    address varchar(255) not null,
    birth_date DATE not null,
    disability bit not null,
    education_form varchar(15) not null,
    gender varchar(10) not null,
    name varchar(100) not null,
    snils varchar(15),
    telephone varchar(18),
    document_id bigint not null,
    passport_id bigint,
    specialty_id bigint,
    status_id bigint not null,
    group_id bigint,
    primary key (id)) engine=InnoDB;

create table student_group (
    group_id bigint not null auto_increment,
    name varchar(8) not null,
    education_plan_id bigint,
    primary key (group_id)) engine=InnoDB;

create table student_orders (
    id bigint not null,
    order_id bigint not null) engine=InnoDB;

create table user (
    id bigint not null auto_increment,
    login varchar(10) not null,
    password varchar(10) not null,
    role varchar(7) not null,
    primary key (id)) engine=InnoDB;

alter table student
    add constraint uk_document_id unique (document_id);

alter table student
    add constraint uk_passport_id unique (passport_id);

alter table education_plan
    add constraint fk_specialty_id
        foreign key (specialty_id)
            references specialty (specialty_id);

alter table grade
    add constraint fk_lesson_id
        foreign key (lesson_id)
            references lesson (lesson_id);

alter table grade
    add constraint fk_student_id
        foreign key (student_id)
            references student (id);

alter table lesson
    add constraint fk_education_plan_id
        foreign key (education_plan_id)
            references education_plan (education_plan_id);

alter table parent
    add constraint fk_parents_id
        foreign key (student_id)
            references student (id);

alter table student
    add constraint fk_document_id
        foreign key (document_id)
            references education_document (document_id);

alter table student
    add constraint fk_passport_id
        foreign key (passport_id)
            references passport (passport_id);

alter table student
    add constraint fk_student_specialty_id
        foreign key (specialty_id)
            references specialty (specialty_id);

alter table student
    add constraint fk_status_id
        foreign key (status_id)
            references status (status_id);

alter table student
    add constraint fk_group_id
        foreign key (group_id)
            references student_group (group_id);

alter table student_group
    add constraint fk_student_group
        foreign key (education_plan_id)
            references education_plan (education_plan_id);

alter table student_orders
    add constraint fk_order_id
        foreign key (order_id)
            references orders (order_id);

alter table student_orders
    add constraint fk_student_orders
        foreign key (id)
            references student (id);