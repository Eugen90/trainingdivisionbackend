package edu.bip.trainingDivision.controllers;

import edu.bip.trainingDivision.domains.Specialty;
import edu.bip.trainingDivision.domains.User;
import edu.bip.trainingDivision.services.SpecialtyService;
import edu.bip.trainingDivision.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/specialty")
public class SpecialtyController {
    private final SpecialtyService specialtyService;
    private final UserService userService;

    public SpecialtyController(SpecialtyService specialtyService, UserService userService) {
        this.specialtyService = specialtyService;
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<?> getSpecialtyList(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                              @RequestHeader(name = "pass", required = false, defaultValue = "") String pass) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            return new ResponseEntity<>(specialtyService.findAll(), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @GetMapping("{id}")
    public ResponseEntity<Specialty> getSpecialty(@PathVariable long id) {
        Specialty specialty = specialtyService.find(id);
        return new ResponseEntity<>(specialty, specialty == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> addSpecialty(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                  @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                  @RequestBody Specialty specialty) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            return new ResponseEntity<>(specialtyService.save(specialty), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PutMapping
    public ResponseEntity<?> updateSpecialty(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                             @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                             @RequestBody Specialty specialty) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            if (specialty.getId() == 0 || specialtyService.find(specialty.getId()) == null)
                return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(specialtyService.save(specialty), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteSpecialty(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                                  @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                                  @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            specialtyService.delete(id);
            return new ResponseEntity<>("Ok", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }
}
