package edu.bip.trainingDivision.controllers;

import edu.bip.trainingDivision.domains.Organisation;
import edu.bip.trainingDivision.domains.User;
import edu.bip.trainingDivision.services.OrganisationService;
import edu.bip.trainingDivision.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/organisation")
public class OrganisationController {
    private final OrganisationService organisationService;
    private final UserService userService;

    public OrganisationController(OrganisationService organisationService, UserService userService) {
        this.organisationService = organisationService;
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<?> findOrganisations(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                               @RequestHeader(name = "pass", required = false, defaultValue = "") String pass) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            return new ResponseEntity<>(organisationService.findAll(), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @GetMapping("{id}")
    public ResponseEntity<?> findOrganisation(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                              @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                              @PathVariable short id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            Organisation organisation = organisationService.find(id);
            if (organisation == null) return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(organisation, HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PostMapping
    public ResponseEntity<?> addOrganisation(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                             @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                             @RequestBody Organisation organisation) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            return new ResponseEntity<>(organisationService.save(organisation), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PutMapping
    public ResponseEntity<?> updateOrganisation(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                                @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                                @RequestBody Organisation organisation) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            if (organisation.getId() == 0 || organisationService.find(organisation.getId()) == null)
                return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(organisationService.save(organisation), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteOrganisation(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                                     @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                                     @PathVariable short id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            organisationService.delete(id);
            return new ResponseEntity<>("Ok", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }
}
