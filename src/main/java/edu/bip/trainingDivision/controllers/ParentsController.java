package edu.bip.trainingDivision.controllers;

import edu.bip.trainingDivision.domains.Parent;
import edu.bip.trainingDivision.domains.User;
import edu.bip.trainingDivision.services.ParentsService;
import edu.bip.trainingDivision.services.StudentService;
import edu.bip.trainingDivision.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/")
public class ParentsController {
    private final ParentsService parentsService;
    private final StudentService studentService;
    private final UserService userService;

    public ParentsController(ParentsService parentsService, StudentService studentService, UserService userService) {
        this.parentsService = parentsService;
        this.studentService = studentService;
        this.userService = userService;
    }

    @GetMapping("student/{studentId}/parents")
    public ResponseEntity<?> getStudentParents(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                              @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                              @PathVariable long studentId) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            return new ResponseEntity<>(parentsService.findAll(studentId), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @GetMapping("parents/{id}")
    public ResponseEntity<?> getParents(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                             @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                             @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            Parent parents = parentsService.find(id);
            return new ResponseEntity<>(parents, parents == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PostMapping("student/{studentId}/parents")
    public ResponseEntity<?> addParents(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                        @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                        @RequestBody Parent parents, @PathVariable long studentId) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            if (studentService.find(studentId) == null) {
                return new ResponseEntity<>("Student not found", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(parentsService.save(parents), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PutMapping("student/parents")
    public ResponseEntity<?> updateParents(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                           @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                           @RequestBody Parent parents) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            if (parents.getId() == 0 || parentsService.find(parents.getId()) == null)
                return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(parentsService.save(parents), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("parents/{id}/")
    public ResponseEntity<String> deleteParents(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                                @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                                @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            parentsService.delete(id);
            return new ResponseEntity<>("Ok", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }
}
