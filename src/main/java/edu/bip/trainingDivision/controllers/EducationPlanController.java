package edu.bip.trainingDivision.controllers;

import edu.bip.trainingDivision.domains.EducationPlan;
import edu.bip.trainingDivision.domains.Specialty;
import edu.bip.trainingDivision.domains.User;
import edu.bip.trainingDivision.services.EducationPlanService;
import edu.bip.trainingDivision.services.SpecialtyService;
import edu.bip.trainingDivision.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/education_plan")
public class EducationPlanController {
    private final EducationPlanService educationPlanService;
    private final SpecialtyService specialtyService;
    private final UserService userService;

    public EducationPlanController(EducationPlanService educationPlanService, SpecialtyService specialtyService, UserService userService) {
        this.educationPlanService = educationPlanService;
        this.specialtyService = specialtyService;
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<?> getEducationPlans(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                               @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                               @RequestParam(name = "specialty", required = false, defaultValue = "0") long specialtyId) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            if (specialtyId == 0) return new ResponseEntity<>(educationPlanService.findAll(), HttpStatus.OK);
            Specialty specialty = specialtyService.find(specialtyId);
            if (specialty == null) return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(educationPlanService.findAll(specialty), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getEducationPlan(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                                          @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                                          @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            EducationPlan educationPlan = educationPlanService.find(id);
            return new ResponseEntity<>(educationPlan, educationPlan == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PostMapping
    public ResponseEntity<?> addEducationPlan(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                          @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                          @RequestBody EducationPlan educationPlan) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            return new ResponseEntity<>(educationPlanService.save(educationPlan), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PutMapping
    public ResponseEntity<?> updateEducationPlan(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                                 @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                                 @RequestBody EducationPlan educationPlan) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            if (educationPlan.getId() == 0 || educationPlanService.find(educationPlan.getId()) == null)
                return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(educationPlanService.save(educationPlan), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteEducationPlan(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                                      @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                                      @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            educationPlanService.delete(id);
            return new ResponseEntity<>("Ok", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }
}
