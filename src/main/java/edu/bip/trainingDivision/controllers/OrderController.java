package edu.bip.trainingDivision.controllers;

import edu.bip.trainingDivision.domains.Order;
import edu.bip.trainingDivision.domains.User;
import edu.bip.trainingDivision.services.OrderService;
import edu.bip.trainingDivision.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/order")
public class OrderController {
    private final OrderService orderService;
    private final UserService userService;

    public OrderController(OrderService orderService, UserService userService) {
        this.orderService = orderService;
        this.userService = userService;
    }

    @GetMapping("/students")
    public ResponseEntity<?> getStudents(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                         @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                         @RequestParam long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            Order order = orderService.find(id);
            if (order == null) return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(orderService.findOrderStudents(order), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @GetMapping
    public ResponseEntity<?> getOrders(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                     @RequestHeader(name = "pass", required = false, defaultValue = "") String pass) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            return new ResponseEntity<>(orderService.findAll(), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getOrder(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                          @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                          @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            Order order = orderService.find(id);
            return new ResponseEntity<>(order, order == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PostMapping
    public ResponseEntity<?> addOrder(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                          @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                          @RequestBody Order order) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            return new ResponseEntity<>(orderService.save(order), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PutMapping
    public ResponseEntity<?> updateOrder(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                         @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                         @RequestBody Order order) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            if (order.getId() == 0 || orderService.find(order.getId()) == null)
                return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(orderService.save(order), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteOrder(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                              @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                              @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            orderService.delete(id);
            return new ResponseEntity<>("Ok", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }
}
