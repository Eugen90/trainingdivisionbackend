package edu.bip.trainingDivision.controllers;

import edu.bip.trainingDivision.domains.Certificate;
import edu.bip.trainingDivision.services.PdfCreatorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@RestController
@RequestMapping("api/certificate")
public class CertificateController {
    private final PdfCreatorService pdfCreatorService;

    public CertificateController(PdfCreatorService pdfCreatorService) {
        this.pdfCreatorService = pdfCreatorService;
    }

    @PutMapping
    public ResponseEntity<?> createCertificate(@RequestBody Certificate certificate) {
        File newFile = pdfCreatorService.createCertificate(certificate);
        byte[] file = new byte[]{};
        try {
            file = Files.readAllBytes(newFile.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(file, HttpStatus.OK);
    }
}
