package edu.bip.trainingDivision.controllers;

import edu.bip.trainingDivision.domains.User;
import edu.bip.trainingDivision.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    private ResponseEntity<?> getUsers(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                    @RequestHeader(name = "pass", required = false, defaultValue = "") String pass) {
        User user = userService.findUser(login, pass);
        if (user != null && user.isAdmin()) return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PostMapping
    private ResponseEntity<?> saveUser(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                       @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                       @RequestBody User user) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.isAdmin()) return new ResponseEntity<>(userService.save(user), HttpStatus.OK);
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("{id}")
    private ResponseEntity<?> deleteUser(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                         @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                         @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (id == 1) return new ResponseEntity<>("Bad request", HttpStatus.BAD_REQUEST);
        if (requestUser != null && requestUser.isAdmin()) {
            userService.delete(id);
            return new ResponseEntity<>("Ok", HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }
}
