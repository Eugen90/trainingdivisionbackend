package edu.bip.trainingDivision.controllers;

import edu.bip.trainingDivision.domains.User;
import edu.bip.trainingDivision.repos.StatusRepo;
import edu.bip.trainingDivision.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/status")
public class StatusController {
    private final StatusRepo statusRepo;
    private final UserService userService;

    public StatusController(StatusRepo statusRepo, UserService userService) {
        this.statusRepo = statusRepo;
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<?> getStatus(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                       @RequestHeader(name = "pass", required = false, defaultValue = "") String pass) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            return new ResponseEntity<>(statusRepo.findAll(), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }
}
