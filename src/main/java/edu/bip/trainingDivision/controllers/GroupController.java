package edu.bip.trainingDivision.controllers;

import edu.bip.trainingDivision.domains.Group;
import edu.bip.trainingDivision.domains.User;
import edu.bip.trainingDivision.services.GroupService;
import edu.bip.trainingDivision.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/group")
public class GroupController {
    private final GroupService groupService;
    private final UserService userService;

    public GroupController(GroupService groupService, UserService userService) {
        this.groupService = groupService;
        this.userService = userService;
    }

    @GetMapping("/students")
    public ResponseEntity<?> getStudents(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                         @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                         @RequestParam long groupId) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            Group group = groupService.find(groupId);
            if (group == null) return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(groupService.findGroupStudents(group), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @GetMapping
    public ResponseEntity<?> getGroups(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                     @RequestHeader(name = "pass", required = false, defaultValue = "") String pass) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            return new ResponseEntity<>(groupService.findAll(), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getGroup(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                          @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                          @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            Group group = groupService.find(id);
            return new ResponseEntity<>(group, group == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PostMapping
    public ResponseEntity<?> addGroup(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                          @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                          @RequestBody Group group) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            return new ResponseEntity<>(groupService.save(group), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PutMapping
    public ResponseEntity<?> updateGroup(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                         @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                         @RequestBody Group group) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            if (group.getId() == 0 || groupService.find(group.getId()) == null)
                return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(groupService.save(group), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteGroup(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                              @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                              @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            groupService.delete(id);
            return new ResponseEntity<>("Ok", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }
}
