package edu.bip.trainingDivision.controllers;

import edu.bip.trainingDivision.domains.EducationPlan;
import edu.bip.trainingDivision.domains.Lesson;
import edu.bip.trainingDivision.domains.User;
import edu.bip.trainingDivision.services.EducationPlanService;
import edu.bip.trainingDivision.services.LessonService;
import edu.bip.trainingDivision.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/lesson")
public class LessonController {
    private final LessonService lessonService;
    private final EducationPlanService educationPlanService;
    private final UserService userService;

    public LessonController(LessonService lessonService, EducationPlanService educationPlanService, UserService userService) {
        this.lessonService = lessonService;
        this.educationPlanService = educationPlanService;
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<?> getLessons(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                        @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                        @RequestParam(name = "plan", required = false, defaultValue = "0") long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            if (id == 0) new ResponseEntity<>(lessonService.findAll(), HttpStatus.OK);
            EducationPlan educationPlan = educationPlanService.find(id);
            if (educationPlan == null) return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(lessonService.findAll(educationPlan), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getLesson(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                            @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                            @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            Lesson lesson = lessonService.find(id);
            return new ResponseEntity<>(lesson, lesson == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PostMapping
    public ResponseEntity<?> addLesson(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                      @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                      @RequestBody List<Lesson> lesson) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            return new ResponseEntity<>(lessonService.saveAll(lesson), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PutMapping
    public ResponseEntity<?> updateLesson(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                          @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                          @RequestBody Lesson lesson) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            if (lesson.getId() == 0 || lessonService.find(lesson.getId()) == null)
                return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(lessonService.save(lesson), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteLesson(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                               @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                               @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            lessonService.delete(id);
            return new ResponseEntity<>("Ok", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }
}
