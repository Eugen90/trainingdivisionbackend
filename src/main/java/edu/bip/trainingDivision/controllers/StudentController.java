package edu.bip.trainingDivision.controllers;

import edu.bip.trainingDivision.domains.Status;
import edu.bip.trainingDivision.domains.Student;
import edu.bip.trainingDivision.domains.User;
import edu.bip.trainingDivision.repos.StatusRepo;
import edu.bip.trainingDivision.services.StudentService;
import edu.bip.trainingDivision.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/student")
public class StudentController {
    private final StudentService studentService;
    private final StatusRepo statusRepo;
    private final UserService userService;

    public StudentController(StudentService studentService, StatusRepo statusRepo, UserService userService) {
        this.studentService = studentService;
        this.statusRepo = statusRepo;
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<?> getStudents(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                         @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                         @RequestParam(name = "status", required = false, defaultValue = "0") long statusId,
                                         @RequestParam(name = "form", required = false, defaultValue = "") String educationForm) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            if (statusId == 0 && educationForm.equals(""))
                return new ResponseEntity<>(studentService.findAll(), HttpStatus.OK);
            Status status = statusRepo.findById(statusId);
            return new ResponseEntity<>(studentService.findAll(status, educationForm), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getStudent(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                        @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                        @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            Student student = studentService.find(id);
            return new ResponseEntity<>(student, student == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PostMapping
    public ResponseEntity<?> addStudent(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                        @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                        @RequestBody List<Student> students) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            return new ResponseEntity<>(studentService.save(students), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PutMapping
    public ResponseEntity<?> updateStudent(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                            @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                            @RequestBody Student student) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            return new ResponseEntity<>(studentService.save(student), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteStudent(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                                @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                                @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            studentService.delete(id);
            return new ResponseEntity<>("Ok", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }
}
