package edu.bip.trainingDivision.controllers;

import edu.bip.trainingDivision.domains.Grade;
import edu.bip.trainingDivision.domains.Student;
import edu.bip.trainingDivision.domains.User;
import edu.bip.trainingDivision.services.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/")
public class GradeController {
    private final GradeService gradeService;
    private final LessonService lessonService;
    private final StudentService studentService;
    private final UserService userService;

    public GradeController(GradeService gradeService, LessonService lessonService, StudentService studentService, UserService userService) {
        this.gradeService = gradeService;
        this.lessonService = lessonService;
        this.studentService = studentService;
        this.userService = userService;
    }

    @GetMapping("student/{studentId}/grades")
    public ResponseEntity<?> getGrades(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                     @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                     @PathVariable long studentId) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            return new ResponseEntity<>(gradeService.findAll(studentId), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @GetMapping("grades/{gradeId}")
    public ResponseEntity<?> getGrade(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                          @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                          @PathVariable long gradeId) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null) {
            Grade grade = gradeService.find(gradeId);
            return new ResponseEntity<>(grade, grade == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PostMapping("grades")
    public ResponseEntity<?> addGrade(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                      @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                      @RequestBody Grade grade) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            if (studentService.find(grade.getStudent().getId()) == null) {
                return new ResponseEntity<>("Student not found", HttpStatus.NOT_FOUND);
            }
            if (lessonService.find(grade.getLesson().getId()) == null) {
                return new ResponseEntity<>("Lesson not found", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(gradeService.save(grade), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @PutMapping("student/{id}/grades")
    public ResponseEntity<?> updateGrade(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                         @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                         @PathVariable long id, @RequestBody Grade grade) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            Student student = studentService.find(id);
            if (student == null) return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            grade.setStudent(student);
            return new ResponseEntity<>(gradeService.save(grade), HttpStatus.OK);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("grades/{id}")
    public ResponseEntity<?> deleteGrade(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                              @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                              @PathVariable long id) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            gradeService.delete(id);
            return new ResponseEntity<>("Ok", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }

    @DeleteMapping("grades")
    public ResponseEntity<?> deleteStudentGrades(@RequestHeader(name = "login", required = false, defaultValue = "") String login,
                                                      @RequestHeader(name = "pass", required = false, defaultValue = "") String pass,
                                                      @RequestParam long studentId) {
        User requestUser = userService.findUser(login, pass);
        if (requestUser != null && requestUser.canWrite()) {
            Student student = studentService.find(studentId);
            gradeService.delete(student);
            return new ResponseEntity<>("Ok", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Access denied", HttpStatus.FORBIDDEN);
    }
}
