package edu.bip.trainingDivision.services;

import edu.bip.trainingDivision.domains.*;
import edu.bip.trainingDivision.repos.StudentRepo;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService implements CrudService<Student> {
    private final StudentRepo studentRepo;
    private final ParentsService parentsService;
    private final EducationPlanService educationPlanService;
    private final GradeService gradeService;

    public StudentService(StudentRepo studentRepo, @Lazy ParentsService parentsService, EducationPlanService educationPlanService, GradeService gradeService) {
        this.studentRepo = studentRepo;
        this.parentsService = parentsService;
        this.educationPlanService = educationPlanService;
        this.gradeService = gradeService;
    }

    @Override
    public Iterable<Student> findAll() {
        return studentRepo.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    public Iterable<Student> findAll(Status status, String educationForm) {
        if (status == null) return studentRepo.findAllByEducationFormOrderByName(educationForm);
        if (educationForm.equals("")) return studentRepo.findAllByStatusOrderByName(status);
        return studentRepo.findAllByEducationFormAndStatusOrderByName(educationForm, status);
    }

    @Override
    public Student find(long id) {
        return studentRepo.findById(id);
    }

    @Override
    public Student save(Student student) {
        if (student.getId() != 0 && student.getStatus().getId() == 1) validateStudentGradesList(student);
        Student savedStudent = (student.getId() == 0)
                ? studentRepo.save(saveNewStudent(student))
                : studentRepo.save(student);
        saveParents(student.getParents(), savedStudent);
        return savedStudent;
    }

    public List<Student> save(List<Student> students) {
        List<Student> savedStudents = new ArrayList<>();
        for (Student student : students) {
            if (student.getId() != 0 && student.getStatus().getId() == 1) validateStudentGradesList(student);
            Student savedStudent = (student.getId() == 0)
                    ? studentRepo.save(saveNewStudent(student))
                    : studentRepo.save(student);
            saveParents(student.getParents(), savedStudent);
            savedStudents.add(savedStudent);
        }
        return savedStudents;
    }

    private void validateStudentGradesList(Student student) {
        Student savedStudent = studentRepo.findById(student.getId());
        //Создает новый список оценок при добавлении в группу или при смене специальности
        if (student.getStudentsGroup() != null && savedStudent.getStudentsGroup() == null ||
                student.getSpecialty().getId() != savedStudent.getSpecialty().getId()) {
            student.setGrades(createStudentGrades(student));
        }
    }

    private List<Grade> createStudentGrades(Student student) {
        EducationPlan plan = educationPlanService.find(student.getStudentsGroup().getEducationPlan().getId());
        List<Grade> grades = new ArrayList<>();
        for (Lesson lesson : plan.getLessonList()) {
            grades.add(new Grade(lesson, student));
        }
        return grades;
    }

    private Student saveNewStudent(Student student) {
        List<Order> orders = student.getOrders();
        student.setOrders(null);
        Student savedStudent = studentRepo.save(student);
        savedStudent.setOrders(saveOrders(orders, savedStudent));
        return savedStudent;
    }

    private void saveParents(List<Parent> parents, Student savedStudent) {
        if (parents != null) {
            for (Parent parent : parents) {
                parent.setStudent(savedStudent);
                parentsService.save(parent);
            }
        }
    }

    private List<Order> saveOrders(List<Order> orders, Student savedStudent) {
        List<Order> orderList = new ArrayList<>();
        if (orders != null) {
            for (Order order : orders) {
                List<Student> students = new ArrayList<>();
                students.add(savedStudent);
                order.setStudents(students);
                orderList.add(order);
            }
        }
        return orderList;
    }

    @Override
    public void delete(long id) {
        deleteStudentOrders(id);
        studentRepo.deleteById(id);
    }

    private void deleteStudentOrders(long id) {
        Student student = studentRepo.findById(id);
        student.setOrders(null);
        studentRepo.save(student);
    }
}
