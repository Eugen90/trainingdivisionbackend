package edu.bip.trainingDivision.services;

import edu.bip.trainingDivision.domains.Specialty;
import edu.bip.trainingDivision.repos.SpecialtyRepo;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class SpecialtyService implements CrudService<Specialty>{
    private final SpecialtyRepo specialtyRepo;

    public SpecialtyService(SpecialtyRepo specialtyRepo) {
        this.specialtyRepo = specialtyRepo;
    }

    @Override
    public Iterable<Specialty> findAll() {
        return specialtyRepo.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Override
    public Specialty find(long id) {
        return specialtyRepo.findById(id);
    }

    @Override
    public Specialty save(Specialty specialty) {
        return specialtyRepo.save(specialty);
    }

    @Override
    public void delete(long id) {
        specialtyRepo.deleteById(id);
    }
}
