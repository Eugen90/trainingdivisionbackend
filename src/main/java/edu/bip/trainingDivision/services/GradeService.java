package edu.bip.trainingDivision.services;

import edu.bip.trainingDivision.domains.Grade;
import edu.bip.trainingDivision.domains.Student;
import edu.bip.trainingDivision.repos.GradeRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GradeService {
    private final GradeRepo gradeRepo;

    public GradeService(GradeRepo gradeRepo) {
        this.gradeRepo = gradeRepo;
    }

    public Iterable<Grade> findAll(long studentId) {
        return gradeRepo.findAllByStudentId(studentId);
    }

    public Grade find(long id) {
        return gradeRepo.findById(id);
    }

    public Grade save(Grade grade) {
        return gradeRepo.save(grade);
    }

    public void delete(long id) {
        gradeRepo.deleteById(id);
    }

    public void delete(Student student) {
        gradeRepo.deleteAllByStudent(student);
    }
}
