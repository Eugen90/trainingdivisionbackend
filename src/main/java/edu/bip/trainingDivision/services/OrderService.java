package edu.bip.trainingDivision.services;

import edu.bip.trainingDivision.domains.Order;
import edu.bip.trainingDivision.domains.Student;
import edu.bip.trainingDivision.repos.OrderRepo;
import edu.bip.trainingDivision.repos.StudentRepo;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.List;

@Service
public class OrderService implements CrudService<Order> {
    private final OrderRepo orderRepo;
    private final StudentRepo studentRepo;

    public OrderService(OrderRepo orderRepo,@Lazy StudentRepo studentRepo) {
        this.orderRepo = orderRepo;
        this.studentRepo = studentRepo;
    }

    public Iterable<Student> findOrderStudents(Order order) {
        return studentRepo.findByOrdersOrderByName(order);
    }

    @Override
    public Iterable<Order> findAll() {
        return orderRepo.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Override
    public Order find(long id) {
        return orderRepo.findById(id);
    }

    @Override
    public Order save(Order order) {
        return orderRepo.save(order);
    }

    @Override
    public void delete(long id) {
        deleteOrderFromStudents(id);
        orderRepo.deleteById(id);
    }

    private void deleteOrderFromStudents(long id) {
        Order deleteOrder = orderRepo.findById(id);
        Iterable<Student> students = studentRepo.findByOrdersOrderByName(deleteOrder);
        for (Student student : students) {
            List<Order> orders = student.getOrders();
            Order order = findOrder(orders, deleteOrder);
            if (order != null) orders.remove(order);
        }
        studentRepo.saveAll(students);
    }

    private Order findOrder(List<Order> orderList, Order order) {
        for (Order item : orderList) {
            if (item.getId() == order.getId()) return item;
        }
        return null;
    }
}
