package edu.bip.trainingDivision.services;

import edu.bip.trainingDivision.domains.EducationPlan;
import edu.bip.trainingDivision.domains.Specialty;
import edu.bip.trainingDivision.repos.EducationPlanRepo;
import org.springframework.stereotype.Service;

@Service
public class EducationPlanService implements CrudService<EducationPlan> {
    private final EducationPlanRepo educationPlanRepo;

    public EducationPlanService(EducationPlanRepo educationPlanRepo) {
        this.educationPlanRepo = educationPlanRepo;
    }

    @Override
    public Iterable<EducationPlan> findAll() {
        return educationPlanRepo.findAll();
    }

    public Iterable<EducationPlan> findAll(Specialty specialty) {
        return educationPlanRepo.findAllBySpecialtyOrderByYearDesc(specialty);
    }

    @Override
    public EducationPlan find(long id) {
        return educationPlanRepo.findById(id);
    }

    @Override
    public EducationPlan save(EducationPlan educationPlan) {
        if (educationPlan.getId() > 0)
            educationPlan.setLessonList(null);
        return educationPlanRepo.save(educationPlan);
    }

    @Override
    public void delete(long id) {
        educationPlanRepo.deleteById(id);
    }
}
