package edu.bip.trainingDivision.services;

import edu.bip.trainingDivision.domains.User;
import edu.bip.trainingDivision.repos.UserRepo;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    final private UserRepo userRepo;

    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public Iterable<User> findAll() {
        return userRepo.findAll();
    }

    public User findUser(String login, String password) {
        return userRepo.findByLoginAndPassword(login, password);
    }

    public User save(User user) {
        return userRepo.save(user);
    }

    public void delete(long id) {
        userRepo.deleteById(id);
    }
}
