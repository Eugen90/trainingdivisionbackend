package edu.bip.trainingDivision.services;

import edu.bip.trainingDivision.domains.*;
import edu.bip.trainingDivision.repos.GroupRepo;
import edu.bip.trainingDivision.repos.StudentRepo;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GroupService implements CrudService<Group>{
    private final GroupRepo groupRepo;
    private final StudentRepo studentRepo;
    private final EducationPlanService educationPlanService;

    public GroupService(GroupRepo groupRepo, StudentRepo studentRepo, EducationPlanService educationPlanService) {
        this.groupRepo = groupRepo;
        this.studentRepo = studentRepo;
        this.educationPlanService = educationPlanService;
    }

    public Iterable<Student> findGroupStudents(Group group) {
        return studentRepo.findAllByStudentsGroupOrderByName(group);
    }

    @Override
    public Iterable<Group> findAll() {
        return groupRepo.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Override
    public Group find(long id) {
        return groupRepo.findById(id);
    }

    @Override
    public Group save(Group group) {
        group.setEducationPlan(educationPlanService.find(group.getEducationPlan().getId()));
        return groupRepo.save(group);
    }

    @Override
    public void delete(long id) {
        deleteStudentsFromGroup(id);
        deleteGroupFromEducationPlan(id);
        groupRepo.deleteById(id);
    }

    private void deleteGroupFromEducationPlan(long id) {
        Group group = groupRepo.findById(id);
        group.setEducationPlan(null);
        groupRepo.save(group);
    }

    private void deleteStudentsFromGroup(long id) {
        Iterable<Student> students = studentRepo.findAllByStudentsGroupOrderByName(groupRepo.findById(id));
        students.forEach(s -> s.setStudentsGroup(null));
        studentRepo.saveAll(students);
    }
}
