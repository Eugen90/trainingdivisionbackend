package edu.bip.trainingDivision.services;

import edu.bip.trainingDivision.domains.Organisation;
import edu.bip.trainingDivision.repos.OrganisationRepo;
import org.springframework.stereotype.Service;

@Service
public class OrganisationService implements CrudService<Organisation> {
    private final OrganisationRepo organisationRepo;

    public OrganisationService(OrganisationRepo organisationRepo) {
        this.organisationRepo = organisationRepo;
    }

    @Override
    public Iterable<Organisation> findAll() {
        return organisationRepo.findAll();
    }

    @Override
    public Organisation find(long id) {
        return organisationRepo.findById((short) id);
    }

    @Override
    public Organisation save(Organisation organisation) {
        return organisationRepo.save(organisation);
    }

    @Override
    public void delete(long id) {
        organisationRepo.deleteById((short) id);
    }
}
