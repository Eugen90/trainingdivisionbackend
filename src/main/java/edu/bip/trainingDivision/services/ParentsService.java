package edu.bip.trainingDivision.services;

import edu.bip.trainingDivision.domains.Parent;
import edu.bip.trainingDivision.repos.ParentsRepo;
import org.springframework.stereotype.Service;

@Service
public class ParentsService {
    private final ParentsRepo parentsRepo;

    public ParentsService(ParentsRepo parentsRepo) {
        this.parentsRepo = parentsRepo;
    }

    public Iterable<Parent> findAll(long studentId) {
        return parentsRepo.findAllByStudentId(studentId);
    }

    public Parent find(long parentId) {
        return parentsRepo.findById(parentId);
    }

    public Parent save(Parent parents) {
        return parentsRepo.save(parents);
    }

    public void delete(long parentId) {
        parentsRepo.deleteById(parentId);
    }
}
