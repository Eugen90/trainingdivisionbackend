package edu.bip.trainingDivision.services;

import edu.bip.trainingDivision.domains.EducationPlan;
import edu.bip.trainingDivision.domains.Lesson;
import edu.bip.trainingDivision.repos.LessonRepo;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LessonService implements CrudService<Lesson>{
    private final LessonRepo lessonRepo;

    public LessonService(LessonRepo lessonRepo) {
        this.lessonRepo = lessonRepo;
    }

    public Iterable<Lesson> findAll() {
        return lessonRepo.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    public Iterable<Lesson> findAll(EducationPlan plan) {
        return lessonRepo.findAllByEducationPlanOrderBySemesterNumber(plan);
    }

    @Override
    public Lesson find(long id) {
        return lessonRepo.findById(id);
    }

    @Override
    public Lesson save(Lesson lesson) {
        return lessonRepo.save(lesson);
    }

    @Override
    public void delete(long id) {
        lessonRepo.deleteById(id);
    }

    public Iterable<Lesson> saveAll(List<Lesson> lesson) {
        return lessonRepo.saveAll(lesson);
    }
}
