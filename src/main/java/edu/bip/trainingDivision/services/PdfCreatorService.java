package edu.bip.trainingDivision.services;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import edu.bip.trainingDivision.domains.Certificate;
import edu.bip.trainingDivision.domains.Organisation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class PdfCreatorService {
    private final OrganisationService organisationService;
    @Value("${upload.path}")
    private String uploadPath;

    public PdfCreatorService(OrganisationService organisationService) {
        this.organisationService = organisationService;
    }

    public void createPdfCertificate(Certificate certificate) {
        Organisation organisation = organisationService.find(certificate.getOrganisationId());
        Document document = new Document(PageSize.A4);
        try {
            PdfWriter.getInstance(document, new FileOutputStream(uploadPath + "/certificate.pdf"));

            document.open();
            BaseFont baseFont = BaseFont.createFont(uploadPath + "/times.ttf", BaseFont.IDENTITY_H , BaseFont.EMBEDDED);
            Font titleFont = new Font(baseFont, 14, Font.BOLD);
            Font headerFont = new Font(baseFont, 10);
            Font bodyFont = new Font(baseFont, 14);
            //Шапка
            document.add(createParagraph(organisation.getDesignation(), bodyFont, Element.ALIGN_CENTER));
            document.add(createParagraph(organisation.getName(), titleFont, Element.ALIGN_CENTER));
            String line = organisation.getAddress() + ", " + organisation.getPostIndex() + ",\n"
                    + "тел./факс. " + organisation.getTelephone() + ", сайт: " + organisation.getWebsite()
                    + ", e-mail: " + organisation.getEmail() + "\n"
                    + "Лицензия " + organisation.getLicense() + " Св-во о гос. аккредитации " + organisation.getAccreditationCertificate()
                    + "\n___________________________________________________________________________________________";
            document.add(createParagraph(line, headerFont, Element.ALIGN_CENTER));
            document.add(createParagraph("\nС П Р А В К А   № " + certificate.getNumber(), titleFont, Element.ALIGN_CENTER));
            //Справка
            line = "\n" + certificate.getBody() + "\n" + certificate.getGrants();
            document.add(createParagraph(line, bodyFont, Element.ALIGN_JUSTIFIED));

            line = "\n" + certificate.getDeadline() + "  " + certificate.getDate() + "\n" + certificate.getOtherInformation()
                    + "\n" + certificate.getPresenter();
            document.add(createParagraph(line, bodyFont, Element.ALIGN_JUSTIFIED));

            line = "\n" + organisation.getDirectorPosition() + createSpaceLine(organisation) + organisation.getDirector();
            document.add(createParagraph(line, bodyFont, Element.ALIGN_LEFT));
        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        }
        document.close();
    }

    private String createSpaceLine(Organisation organisation) {
        int spacesCount = 130 - (organisation.getDirector().length() + organisation.getDirectorPosition().length() * 2);
        StringBuilder spaces = new StringBuilder();
        for (int i = 0; i < spacesCount; i++) {
            spaces.append(" ");
        }
        return spaces.toString();
    }

    private Paragraph createParagraph(String text, Font font, int alignment) {
        Paragraph paragraph = new Paragraph(text, font);
        paragraph.setAlignment(alignment);
        return paragraph;
    }

    public File createCertificate(Certificate certificate) {
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
        String certificateName = uploadPath + "/certificate.pdf";
        File certificateFile = new File(certificateName);
        createPdfCertificate(certificate);
        return certificateFile;
    }
}
