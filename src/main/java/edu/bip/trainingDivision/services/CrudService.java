package edu.bip.trainingDivision.services;

import edu.bip.trainingDivision.domains.Student;
import org.springframework.stereotype.Service;

public interface CrudService<T> {
    Iterable<T> findAll();
    T find(long id);
    T save(T t);
    void delete(long id);
}
