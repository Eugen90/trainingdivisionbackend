package edu.bip.trainingDivision.domains;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@RequiredArgsConstructor
public class GradesToJson implements Serializable {
    private long id;
    private short mark;
    private short semesterNumber;
    private Lesson lesson;
    private Student student;

    public GradesToJson(long id, short mark, short semesterNumber, Lesson lesson, Student student) {
        this.id = id;
        this.mark = mark;
        this.semesterNumber = semesterNumber;
        this.lesson = lesson;
        this.student = student;
    }
}
