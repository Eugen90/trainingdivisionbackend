package edu.bip.trainingDivision.domains;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class EducationDocument implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "document_id")
    private long id;

    @NotNull
    @Length(max = 50)
    private String name;

    @NotNull
    @Length(max = 20)
    private String number;

    @NotNull
    @Column(columnDefinition = "DATE")
    private LocalDate issuedDate;

    @NotNull
    @Length(max = 70)
    private String institutionName;
}
