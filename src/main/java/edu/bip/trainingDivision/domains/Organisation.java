package edu.bip.trainingDivision.domains;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
public class Organisation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "organisation_id")
    private short id;

    @NotNull
    @Length(max = 80)
    private String designation;

    @NotNull
    @Length(max = 10)
    private String abbreviation;

    @NotNull
    @Length(max = 50)
    private String name;

    @NotNull
    @Length(max = 80)
    private String address;

    @NotNull
    @Length(max = 8)
    private String postIndex;

    @NotNull
    @Length(max = 40)
    private String telephone;

    @NotNull
    @Length(max = 50)
    private String website;

    @NotNull
    @Email
    private String email;

    @NotNull
    @Length(max = 40)
    private String license;

    @NotNull
    @Length(max = 40)
    private String accreditationCertificate;

    @NotNull
    @Length(max = 40)
    private String director;

    @NotNull
    @Length(max = 40)
    private String directorPosition;
}
