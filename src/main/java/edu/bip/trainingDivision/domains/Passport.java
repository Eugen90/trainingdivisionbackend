package edu.bip.trainingDivision.domains;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Passport implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "passport_id")
    private long id;

    @NotNull
    @Length(max = 12)
    private String number;

    @NotNull
    @Length(max = 150)
    private String issuedBy;

    @NotNull
    @Column(columnDefinition = "DATE")
    private LocalDate issuedDate;
}
