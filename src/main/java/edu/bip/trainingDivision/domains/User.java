package edu.bip.trainingDivision.domains;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Length(min = 4, max = 10)
    private String login;

    @NotNull
    @Length(min = 4, max = 10)
    private String password;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role;

    public boolean isAdmin() {
        return role == Role.ADMIN;
    }

    public boolean canWrite() {
        return role == Role.WRITE || role == Role.ADMIN;
    }
}
