package edu.bip.trainingDivision.domains;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "student_group")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_id")
    private long id;

    @NotNull
    @Length(max = 8)
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "education_plan_id")
    private EducationPlan educationPlan;
}
