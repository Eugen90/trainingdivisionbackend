package edu.bip.trainingDivision.domains;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
public class Specialty {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "specialty_id")
    private long id;

    @NotNull
    @Length(max = 150)
    private String name;

    @NotNull
    @Column(columnDefinition = "DATE")
    private LocalDate endingDate;

    @NotNull
    @Min(0)
    @Max(5)
    private short trainingLength;
}
