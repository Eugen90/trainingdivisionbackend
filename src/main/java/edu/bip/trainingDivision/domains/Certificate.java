package edu.bip.trainingDivision.domains;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@RequiredArgsConstructor
public class Certificate implements Serializable {
    private String number;
    private short organisationId;
    private String body;
    private String grants;
    private String deadline;
    private String date;
    private String otherInformation;
    private String presenter;
}
