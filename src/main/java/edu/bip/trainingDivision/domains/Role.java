package edu.bip.trainingDivision.domains;

public enum Role {
    ADMIN,
    WRITE,
    READ;

    Role() {
    }
}
