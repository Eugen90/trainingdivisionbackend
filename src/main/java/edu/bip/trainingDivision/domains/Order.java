package edu.bip.trainingDivision.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private long id;

    @NotNull
    @Length(max = 22)
    private String name;

    @NotNull
    @Length(max = 25)
    private String number;

    @NotNull
    @Column(columnDefinition = "DATE")
    private LocalDate date;

    @JsonIgnore
    @ManyToMany(mappedBy = "orders")
    private List<Student> students;
}
