package edu.bip.trainingDivision.domains;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Student implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Length(max = 100)
    private String name;

    @NotNull
    @Column(columnDefinition = "DATE")
    private LocalDate birthDate;

    @NotNull
    private String address;

    @Length(max = 18)
    private String telephone;

    @NotNull
    private boolean disability;

    @NotNull
    @Length(max = 10)
    private String gender;

    @NotNull
    @Length(max = 15)
    private String educationForm;

    @Length(max = 15)
    private String snils;

    @ManyToOne
    @JoinColumn(name = "specialty_id")
    private Specialty specialty;

    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group studentsGroup;

    @ManyToOne
    @JoinColumn(name = "status_id")
    @NotNull
    private Status status;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "student_orders",
            joinColumns = { @JoinColumn(name = "id") },
            inverseJoinColumns = { @JoinColumn(name = "order_id") }
    )
    private List<Order> orders;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<Parent> parents;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "student")
    private List<Grade> grades;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "document_id", unique = true, nullable = false)
    private EducationDocument educationDocument;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "passport_id", unique = true)
    private Passport passport;

    public Student(long id, String name) {
        this.id = id;
        this.name = name;
    }
}
