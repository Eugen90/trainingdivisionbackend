package edu.bip.trainingDivision.repos;

import edu.bip.trainingDivision.domains.Organisation;
import org.springframework.data.repository.CrudRepository;

public interface OrganisationRepo extends CrudRepository<Organisation, Short> {
    Organisation findById(short id);
}
