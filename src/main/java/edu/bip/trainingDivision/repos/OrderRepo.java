package edu.bip.trainingDivision.repos;

import edu.bip.trainingDivision.domains.Order;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepo extends CrudRepository<Order, Long> {
    Order findById(long id);
    Iterable<Order> findAll(Sort number);
}
