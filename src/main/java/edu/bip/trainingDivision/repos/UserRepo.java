package edu.bip.trainingDivision.repos;

import edu.bip.trainingDivision.domains.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepo extends CrudRepository<User, Long> {
    User findByLoginAndPassword(String login, String password);
}
