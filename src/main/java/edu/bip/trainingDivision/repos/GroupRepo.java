package edu.bip.trainingDivision.repos;

import edu.bip.trainingDivision.domains.Group;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

public interface GroupRepo extends CrudRepository<Group, Long> {
    Group findById(long id);
    Iterable<Group> findAll(Sort name);
}
