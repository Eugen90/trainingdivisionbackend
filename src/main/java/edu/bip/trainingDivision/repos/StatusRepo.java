package edu.bip.trainingDivision.repos;

import edu.bip.trainingDivision.domains.Status;
import org.springframework.data.repository.CrudRepository;

public interface StatusRepo extends CrudRepository<Status, Long> {
    Status findById(long id);
}
