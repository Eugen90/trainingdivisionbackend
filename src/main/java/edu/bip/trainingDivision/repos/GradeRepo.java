package edu.bip.trainingDivision.repos;

import edu.bip.trainingDivision.domains.Grade;
import edu.bip.trainingDivision.domains.Student;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface GradeRepo extends CrudRepository<Grade, Long> {
    Iterable<Grade> findAllByStudentId(long id);
    Grade findById(long id);
    @Transactional
    @Modifying
    @Query("DELETE FROM Grade g where g.student=:student")
    void deleteAllByStudent(Student student);
}
