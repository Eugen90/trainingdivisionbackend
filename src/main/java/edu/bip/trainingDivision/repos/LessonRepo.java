package edu.bip.trainingDivision.repos;

import edu.bip.trainingDivision.domains.EducationPlan;
import edu.bip.trainingDivision.domains.Lesson;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

public interface LessonRepo extends CrudRepository<Lesson, Long> {
    Lesson findById(long id);
    Iterable<Lesson> findAll(Sort name);
    Iterable<Lesson> findAllByEducationPlanOrderBySemesterNumber(EducationPlan educationPlan);
}
