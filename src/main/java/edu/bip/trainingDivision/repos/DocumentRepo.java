package edu.bip.trainingDivision.repos;

import edu.bip.trainingDivision.domains.EducationDocument;
import org.springframework.data.repository.CrudRepository;

public interface DocumentRepo extends CrudRepository<EducationDocument, Long> {
}
