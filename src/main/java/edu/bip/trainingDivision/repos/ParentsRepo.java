package edu.bip.trainingDivision.repos;

import edu.bip.trainingDivision.domains.Parent;
import org.springframework.data.repository.CrudRepository;

public interface ParentsRepo extends CrudRepository<Parent,Long> {
    Iterable<Parent> findAllByStudentId(long id);
    Parent findById(long id);
}
