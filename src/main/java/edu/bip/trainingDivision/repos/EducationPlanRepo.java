package edu.bip.trainingDivision.repos;

import edu.bip.trainingDivision.domains.EducationPlan;
import edu.bip.trainingDivision.domains.Specialty;
import org.springframework.data.repository.CrudRepository;

public interface EducationPlanRepo extends CrudRepository<EducationPlan, Long> {
    EducationPlan findById(long id);
    Iterable<EducationPlan> findAllBySpecialtyOrderByYearDesc(Specialty specialty);
}
