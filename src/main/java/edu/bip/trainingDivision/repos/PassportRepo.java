package edu.bip.trainingDivision.repos;

import edu.bip.trainingDivision.domains.Passport;
import org.springframework.data.repository.CrudRepository;

public interface PassportRepo extends CrudRepository<Passport, Long> {
}
