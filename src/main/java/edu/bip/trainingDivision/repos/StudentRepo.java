package edu.bip.trainingDivision.repos;

import edu.bip.trainingDivision.domains.Group;
import edu.bip.trainingDivision.domains.Order;
import edu.bip.trainingDivision.domains.Status;
import edu.bip.trainingDivision.domains.Student;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface StudentRepo extends CrudRepository<Student, Long> {
    Student findById(long id);
    Iterable<Student> findByOrdersOrderByName(Order order);
    Iterable<Student> findAllByStudentsGroupOrderByName(Group group);
    Iterable<Student> findAll(Sort name);
    Iterable<Student> findAllByStatusOrderByName(Status status);
    Iterable<Student> findAllByEducationFormOrderByName(String educationForm);
    Iterable<Student> findAllByEducationFormAndStatusOrderByName(String educationForm, Status status);
}
