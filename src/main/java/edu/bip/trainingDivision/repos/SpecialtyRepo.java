package edu.bip.trainingDivision.repos;

import edu.bip.trainingDivision.domains.Specialty;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

public interface SpecialtyRepo extends CrudRepository<Specialty, Long> {
    Specialty findById(long id);
    Iterable<Specialty> findAll(Sort name);
}
